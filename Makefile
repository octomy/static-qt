.SUFFIXES: .js .css .html .svg .jpeg .png .htm
.PHONY: all code-quality test setup up req

.EXPORT_ALL_VARIABLES:
SHELL:=/bin/bash

DOCKER_BUILDKIT:=1

ROOT_DIR:=$(shell dirname "$(realpath $(lastword $(MAKEFILE_LIST)))")
TESTS_DIR:=${ROOT_DIR}/tests
REQUIREMENTS_DIR:=${ROOT_DIR}/requirements
CODE_QUALITY_DIR:=${ROOT_DIR}/code_quality
FK_BARE_VERSION:=$(shell cat "$(ROOT_DIR)/VERSION")

CI_COMMIT_REF_NAME?=$(shell git rev-parse --abbrev-ref HEAD)
FK_GIT_ACTUAL_BRANCH:=$(CI_COMMIT_REF_NAME)
FK_GIT_HASH?=$(shell git rev-parse HEAD)
FK_GIT_HASH_SHORT?=$(shell git rev-parse --short HEAD)
FK_BUILD_DATE?=$(shell date -u +'%Y-%m-%dT%H:%M:%SZ')

# See https://gitlab.com/octomy/common#versioning for details on versioning scheme
FK_VERSION:=$(shell \
	branch=$(FK_GIT_ACTUAL_BRANCH); \
	version='$(FK_BARE_VERSION)'; \
	if [[ "$${branch}" == production ]]; then \
		echo "$${version}"; \
	elif [[ "$${branch}" == beta ]]; then \
		echo "$${version}-beta"; \
	elif [[ "$${branch}" == stage ]]; then \
		echo "$${version}-stage"; \
	elif [[ "$${branch}" == stage-* ]] && [[ "$${branch:6}" != "" ]] ; then \
		echo "$${version}-$${branch:6}"; \
	else \
		echo "$${version}-test-$${branch//[      ]/_}"; \
	fi )


VENV_NAME:=fk_venv


WEBMINIFY_NPROC:=120
WEBMINIFY_CMD:=web-minify
BEAUTIFY_OPTS:= --mode beautify --overwrite --nproc $(WEBMINIFY_NPROC)
#--disable-type-html
MINIFY_OPTS:=   --mode minify   --overwrite --nproc $(WEBMINIFY_NPROC) --gzip --on-change 

FK_PROJECT_GROUP_BASE_NAME:=firstkiss
FK_PROJECT_BASE_NAME:=static-qt
FK_PROJECT_NAME:=$(FK_PROJECT_GROUP_BASE_NAME)/$(FK_PROJECT_BASE_NAME)

FK_DOCKER_REGISTRY_NAME:=${FK_DOCKER_REGISTRY_NAME}
FK_DOCKER_REGISTRY_FILENAME:=$(FK_DOCKER_REGISTRY_NAME)_credentials.yaml
FK_DOCKER_REGISTRY_DOMAIN:=${FK_DOCKER_REGISTRY_DOMAIN}
FK_DOCKER_REGISTRY_URL:=https://$(FK_DOCKER_REGISTRY_DOMAIN)
FK_DOCKER_REGISTRY_PROJECT_URL:=$(FK_DOCKER_REGISTRY_URL)/$(FK_PROJECT_GROUP_BASE_NAME)
FK_DOCKER_REGISTRY_PROJECT_DOMAIN:=$(FK_DOCKER_REGISTRY_DOMAIN)/$(FK_PROJECT_GROUP_BASE_NAME)
FK_DOCKER_REGISTRY_USERNAME:=${FK_DOCKER_REGISTRY_USERNAME}
FK_DOCKER_REGISTRY_PASSWORD:=${FK_DOCKER_REGISTRY_PASSWORD}

FK_PYPI_REGISTRY_URL_BASE:=gitlab.com/api/v4/projects/23170380/packages/pypi/simple
FK_PYPI_REGISTRY_USERNAME:=__token__
FK_PYPI_REGISTRY_PASSWORD:=${FK_PYPI_TOKEN}
FK_PYPI_REGISTRY_URL:=https://$(FK_PYPI_REGISTRY_USERNAME):$(FK_PYPI_REGISTRY_PASSWORD)@$(FK_PYPI_REGISTRY_URL_BASE)


FK_NAMESPACE:=merchbot-$(FK_GIT_ACTUAL_BRANCH)

GITLAB_CACHE_DIR:=$(shell pwd)/gitlab_cache


GOOGLE_APPLICATION_CREDENTIALS_EMAIL:=${GOOGLE_APPLICATION_CREDENTIALS_EMAIL}
GOOGLE_APPLICATION_CREDENTIALS:=${GOOGLE_APPLICATION_CREDENTIALS}
GOOGLE_APPLICATION_CREDENTIALS_TEMP:=/tmp/GOOGLE_APPLICATION_CREDENTIALS.secret



# Information
###################################################################


all: help

info:
	@echo "ROOT_DIR=$(ROOT_DIR)"
	@echo "FK_BARE_VERSION=$(FK_VERSION)"
	@echo "FK_VERSION=$(FK_VERSION)"
	@echo "TESTS_DIR=$(TESTS_DIR)"
	@echo "REQUIREMENTS_DIR=$(REQUIREMENTS_DIR)"
	@echo "CODE_QUALITY_DIR=$(CODE_QUALITY_DIR)"
	@echo "STATIC_SRC=$(STATIC_SRC)"
	@echo "STATIC_DST=$(STATIC_DST)"
	@echo "TEMPLATE_SRC=$(TEMPLATE_SRC)"
	@echo "TEMPLATE_DST=$(TEMPLATE_DST)"
	@echo "SQL_SRC=$(SQL_SRC)"
	@echo "WEBMINIFY_CMD=$(WEBMINIFY_CMD)"
	@echo "BEAUTIFY_OPTS=$(BEAUTIFY_OPTS)"
	@echo "MINIFY_OPTS=$(MINIFY_OPTS)"
	@echo "FK_PROJECT_GROUP_BASE_NAME=$(FK_PROJECT_GROUP_BASE_NAME)"
	@echo "FK_PROJECT_BASE_NAME=$(FK_PROJECT_BASE_NAME)"
	@echo "FK_DOCKER_REGISTRY_NAME=$(FK_DOCKER_REGISTRY_NAME)"
	@echo "FK_DOCKER_REGISTRY_FILENAME=$(FK_DOCKER_REGISTRY_FILENAME)"
	@echo "FK_DOCKER_REGISTRY_DOMAIN=$(FK_DOCKER_REGISTRY_DOMAIN)"
	@echo "FK_DOCKER_REGISTRY_URL=$(FK_DOCKER_REGISTRY_URL)"
	@echo "FK_DOCKER_REGISTRY_PROJECT_URL=$(FK_DOCKER_REGISTRY_PROJECT_URL)"
	@echo "FK_DOCKER_REGISTRY_PROJECT_DOMAIN=$(FK_DOCKER_REGISTRY_PROJECT_DOMAIN)"
	@echo "FK_DOCKER_REGISTRY_USERNAME=$(FK_DOCKER_REGISTRY_USERNAME)"
	@echo "FK_DOCKER_REGISTRY_PASSWORD=$(FK_DOCKER_REGISTRY_PASSWORD)"
	@echo "FK_PYPI_REGISTRY_URL_BASE=$(FK_PYPI_REGISTRY_URL_BASE)"
	@echo "FK_PYPI_REGISTRY_USERNAME=$(FK_PYPI_REGISTRY_USERNAME)"
	@echo "FK_PYPI_REGISTRY_PASSWORD=$(FK_PYPI_REGISTRY_PASSWORD)"
	@echo "FK_PYPI_REGISTRY_URL=$(FK_PYPI_REGISTRY_URL)"
	@echo "FK_GIT_ACTUAL_BRANCH=$(FK_GIT_ACTUAL_BRANCH)"
	@echo "CI_COMMIT_REF_NAME=$(CI_COMMIT_REF_NAME)"
	@echo "CI_COMMIT_TAG=$(CI_COMMIT_TAG)"
	@echo "CI_PROJECT_DIR=$(CI_PROJECT_DIR)"
	@echo "MINIFY_SOURCES=${MINIFY_SOURCES}"
	@echo "GOOGLE_APPLICATION_CREDENTIALS_EMAIL=$(GOOGLE_APPLICATION_CREDENTIALS_EMAIL)"
	@echo "GOOGLE_APPLICATION_CREDENTIALS=$(GOOGLE_APPLICATION_CREDENTIALS)"
	@echo "GOOGLE_APPLICATION_CREDENTIALS_TEMP=$(GOOGLE_APPLICATION_CREDENTIALS_TEMP)"
	@echo "GITLAB_CACHE_DIR=$(GITLAB_CACHE_DIR)"
	@echo "VENV_NAME=$(VENV_NAME)"

env:
	@echo "=== ENV"
	export -p
	@echo " "

ver:
	@echo "=== DOCKER TAG / PYPI VERSION:"
	@echo "$(FK_VERSION)"
	@echo " "
	@echo "=== PYTHON & PIP:"
	@. ~/.venv/$(VENV_NAME)/bin/activate;\
	 python --version || python3 --version || echo "NO PYTHON";\
	 pip --version || pip3 --version || echo "NO PIP"
	@echo " "
	@echo "=== DOCKER:"
	@docker --version || echo "NO DOCKER"
	@echo " "
	@echo "=== DOCKER-COMPOSE:"
	@docker-compose --version || echo "NO DOCKER-COMPOSE"
	@echo " "
	@echo "=== KUBECTL:"
	@kubectl version --client || echo "NO KUBECTL"
	@echo " "
	@echo "=== GCLOUD:"
	@gcloud --version || echo "NO GCLOUD"
	@echo " "
	@echo "=== WHICH:"
	@which docker || echo "NO WHICH DOCKER"
	@which docker-compose || echo "NO WHICH DOCKER-COMPOSE"
	@which kubectl || echo "NO WHICH KUBECTL"
	@which gcloud || echo "NO WHICH GCLOUD"
	@echo " "

# Utility
###################################################################

#=============================
# Sanity checks


# See https://cloud.google.com/docs/authentication/production#automatically
gc-sanity-check:
	@if [ -z "$${GOOGLE_APPLICATION_CREDENTIALS_EMAIL}" ]; then \
		echo "ERROR: GOOGLE_APPLICATION_CREDENTIALS_EMAIL environment variable not set, terminating"; \
		echo "       It should be set in gitlab -> settings -> CI/CD -> variables"; \
		echo "       along with GOOGLE_APPLICATION_CREDENTIALS"; \
		exit 1; \
	fi
	@if [ -z "$${GOOGLE_APPLICATION_CREDENTIALS}" ]; then \
		echo "ERROR: GOOGLE_APPLICATION_CREDENTIALS environment variable not set, terminating"; \
		echo "       It should be set in gitlab -> settings -> CI/CD -> variables"; \
		echo "       along with GOOGLE_APPLICATION_CREDENTIALS_EMAIL"; \
		exit 1; \
	fi



#=============================
# conf management


# Distribute config to expected location of other repos
conf-cutter:
	cp -a config.json "../octomy-batch/config.json" || echo "Problem copying to ../octomy-batch/config.json"

conf-diff:
	@echo " "
	@echo "################################################################"
	diff --color -u "../octomy-batch/config.json" "config.json" || echo ""


# Python requirements management
###################################################################

req-prep:
	pip install --upgrade pip pip-tools

req-src:
	cd "$(REQUIREMENTS_DIR)"; \
	pip-compile --no-header --no-emit-index-url --output-file=requirements.txt \
		requirements.in; \
	pip-compile --no-header --no-emit-index-url --output-file=test_requirements.txt \
		requirements.in \
		test_requirements.in; \

req-install:
	cd "$(REQUIREMENTS_DIR)"; \
	pip install -r requirements.txt

req-install-test:
	cd "$(REQUIREMENTS_DIR)"; \
	pip install -r test_requirements.txt

req: req-prep req-src

uninstall:
	pip uninstall -y $(FK_PROJECT_BASE_NAME);

# Build and re-install package locally
setup: uninstall
	pip install --verbose -e $(ROOT_DIR);


# Code quality
###################################################################

code-quality: docker-registry-sanity-check
	cd "${CODE_QUALITY_DIR}" && make all

# Web minify
###################################################################

beautify-templates:
	$(WEBMINIFY_CMD) $(TEMPLATE_SRC) $(BEAUTIFY_OPTS)
	
beautify-templates-force:
	$(WEBMINIFY_CMD) $(TEMPLATE_SRC) $(BEAUTIFY_OPTS) --force

beautify-templates-dry:
	$(WEBMINIFY_CMD) $(TEMPLATE_SRC) $(BEAUTIFY_OPTS) --dry-run --verbose

beautify-static:
	$(WEBMINIFY_CMD) $(STATIC_SRC) $(BEAUTIFY_OPTS)
	
beautify-static-force:
	$(WEBMINIFY_CMD) $(STATIC_SRC) $(BEAUTIFY_OPTS) --force

beautify-static-dry:
	$(WEBMINIFY_CMD) $(STATIC_SRC) $(BEAUTIFY_OPTS) --dry-run --verbose

beautify-sql:
	$(WEBMINIFY_CMD) $(SQL_SRC) $(BEAUTIFY_OPTS)

beautify-sql-force:
	$(WEBMINIFY_CMD) $(SQL_SRC) $(BEAUTIFY_OPTS) --force

beautify-sql-dry:
	$(WEBMINIFY_CMD) $(SQL_SRC) $(BEAUTIFY_OPTS) --dry-run --verbose

beautify: beautify-templates beautify-static beautify-sql

beautify-force: beautify-templates-force beautify-static-force beautify-sql-force

beautify-dry: beautify-templates-dry beautify-static-dry beautify-sql-dry

minify-templates:
	$(WEBMINIFY_CMD) $(TEMPLATE_SRC) --output $(TEMPLATE_DST) $(MINIFY_OPTS)

minify-templates-force:
	$(WEBMINIFY_CMD) $(TEMPLATE_SRC) --output $(TEMPLATE_DST) $(MINIFY_OPTS) --force

minify-templates-dry:
	$(WEBMINIFY_CMD) $(TEMPLATE_SRC) --output $(TEMPLATE_DST) $(MINIFY_OPTS) --dry-run --verbose

minify-static:
	$(WEBMINIFY_CMD) $(STATIC_SRC) --output $(STATIC_DST) $(MINIFY_OPTS)

minify-static-force:
	$(WEBMINIFY_CMD) $(STATIC_SRC) --output $(STATIC_DST) $(MINIFY_OPTS) --force

minify-static-dry:
	$(WEBMINIFY_CMD) $(STATIC_SRC) --output $(STATIC_DST) $(MINIFY_OPTS) --dry-run --verbose


minify: minify-templates minify-static

minify-force: minify-templates-force minify-static-force

minify-dry: minify-templates-dry minify-static-dry


webminify-html-whitespace-cleanup:
	find templates/ -type f -name *.html -exec sed "s/                                                        //g" -i {} \;



# Docker
###################################################################


#=============================
# Sanity checks

docker-registry-sanity-check:
	@if [ -z "$${FK_DOCKER_REGISTRY_DOMAIN}" ]; then \
		echo "ERROR: FK_DOCKER_REGISTRY_DOMAIN not set, terminating"; \
		echo "       It should be set in gitlab -> settings -> CI/CD -> variables"; \
		echo "       along with FK_DOCKER_REGISTRY_USERNAME and FK_DOCKER_REGISTRY_PASSWORD"; \
		exit 1; \
	fi


#=============================
# DOCKER-LOGIN

docker-login: docker-registry-sanity-check
	@echo "Choose either docker-login-gitlab or docker-login-local"

docker-login-gitlab: docker-registry-sanity-check
	echo  "${CI_REGISTRY_PASSWORD}" | docker login -u "${CI_REGISTRY_USER}" --password-stdin "${CI_REGISTRY}"

docker-login-local: docker-registry-sanity-check
	@echo "FK_DOCKER_REGISTRY_USERNAME=$(FK_DOCKER_REGISTRY_USERNAME)"
	@echo "FK_DOCKER_REGISTRY_PASSWORD=$(FK_DOCKER_REGISTRY_PASSWORD)"
	echo "$(FK_DOCKER_REGISTRY_PASSWORD)" | docker login -u "$(FK_DOCKER_REGISTRY_USERNAME)" --password-stdin  "$(FK_DOCKER_REGISTRY_URL)"


#=============================
# DOCKER-BUILD

docker-build: docker-registry-sanity-check
	DOCKER_BUILDKIT=1 docker build \
	--cache-from $(FK_DOCKER_REGISTRY_PROJECT_DOMAIN)/$(FK_PROJECT_BASE_NAME) \
	--file ${ROOT_DIR}/Dockerfile \
	--pull \
	--build-arg FK_PYPI_TOKEN=${FK_PYPI_TOKEN} \
	--build-arg BUILD_DATE=${FK_BUILD_DATE} \
	--build-arg VCS_REF=${FK_GIT_HASH_SHORT} \
	--build-arg BUILD_VERSION=${FK_VERSION} \
	--tag $(FK_DOCKER_REGISTRY_PROJECT_DOMAIN)/$(FK_PROJECT_BASE_NAME):${FK_VERSION} \
	--progress=plain \
	${ROOT_DIR}


#=============================
# DOCKER-PUSH


docker-push: docker-registry-sanity-check
	DOCKER_BUILDKIT=1 docker push "$(FK_DOCKER_REGISTRY_PROJECT_DOMAIN)/$(FK_PROJECT_BASE_NAME):${FK_VERSION}"

#=============================
# DOCKER-PULL


docker-pull: docker-registry-sanity-check
	DOCKER_BUILDKIT=1 docker pull $(FK_DOCKER_REGISTRY_PROJECT_DOMAIN)/$(FK_PROJECT_BASE_NAME)


docker-prune:
	DOCKER_BUILDKIT=1 docker system prune -af


docker-test:
	echo "No test implemented"

docker-wtf:
	@[ "" != "$(FK_DOCKER_REGISTRY_DOMAIN)" ] || echo "Missing value for FK_DOCKER_REGISTRY_DOMAIN"

dev:
	docker-compose up --build --remove-orphans

postgres-prune:
	sudo rm -rf volumes/postgres || echo "No postgres volume to delete"
	mkdir -p volumes/postgres

# User interface
###################################################################


test: docker-registry-sanity-check
	cd "${TESTS_DIR}" && make all

wtf: docker-wtf gke-wtf


# Help
###############################################################

help:
	@echo ""
	@echo " Convenience makefile for FK tools"
	@echo " ---------------------------------"
	@echo ""
	@echo "  Preparation:"
	@echo ""
	@echo "    make apt-prep            - Prepare environment for development in apt based linux (debian comaptible)"
	@echo "    make apk-prep            - Prepare environment for development in apk based linux (alpine comaptible)"
	@echo "    make fk                  - Rebuild fk python library"
	@echo "    make minify              - Rebuild and compress static content from source"
	@echo ""
	@echo "  Information output:"
	@echo ""
	@echo "    make ver                 - Lists current tool versions"
	@echo "    make info                - Lists internal variables"
	@echo "    make env                 - Lists environment variables"
	@echo "    make wtf                 - Detect errors in current environment"
	@echo "    make req                 - Rebuild pinned versions in *requirements.txt from *requirements.in"
	@echo "    make tests               - Run (almost) all tests. NOTE: For more options see tests/Makefile"
	@echo "    make code-quality        - Run most common code quality tools. NOTE: For more options see code_quality/Makefile"
	@echo ""
	@echo "  configuration management commands:"
	@echo ""
	@echo "    make conf-cutter         - Copy configuration files opportunisticallty to all subprojects in same folder"
	@echo "    make conf-diff           - Diff configuration files opportunisticallty to all subprojects in same folder"
	@echo ""
	@echo "  Docker commands:"
	@echo "    make docker-login        - Login to docker registry"
	@echo "    make docker-build        - Build the all the images of the application"
	@echo "    make docker-up           - Bring up cluster locally with docker-compose (dev only)."
	@echo "                               TIP: the docker-build command will be much faster at building so run it first"
	@echo "    make docker-down         - Bring down cluster locally with docker-compose (dev only)"
	@echo "    make docker-push         - Push all images to docker image repository"
	@echo ""

