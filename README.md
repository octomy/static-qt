[![pipeline status](https://gitlab.com/octomy/static-qt/badges/production/pipeline.svg)](https://gitlab.com/octomy/static-qt/-/commits/production)

# About static-qt
<img src="https://gitlab.com/octomy/static-qt/-/raw/production/design/logo.png" width="64px" alt="static-qt logo"/>

Static Qt builds in Docker

[Go to official project-page](https://gitlab.com/octomy/static-qt)


`static-qt` aims to make building `Qt5.x` and `Qt6.x` into libraries inside Docker convenient for both static and dynamic linkage for a list of supported platforms possible.

`static-qt` was made to support the building of [OctoMY™](https://www.octomy.org "The open-source, easy-to-use, state-of-the-art robot software ") due to OctoMY™'s inherent need for controlling the Qt build to perfection. We hope this can benefit others!


# What version of Qt does static-qt aim to support?
 - Qt5.12.x and later
 - Qt6.4.x and later


# What platforms does static-qt aim to support?
Idealy all platforms supported by Qt but main focus is on the following targets which are derived from OctoMY:


| Platform   |       | Status |
|------------|-------|--------|
| Debian | Desktop | ✅ |
| Gentoo | Desktop | ❌ |
| Alpine | Server | ❌ |
| Windows | Desktop | ❌ |
| Android | Mobile | ❌ |
| OSX | Desktop | ❌ |
| iOS | Mobile | ❌ |


# How is static-qt supposed to be used?

There are two scenarios which may be useful for you

## Scenario 1: Embedded
1. Pull image (fast) -or- build image from Docker (takes time)
2. Create new image that is derived from the version you want to use
3. pull code and build, Qt libs will be in default include path

## Scenario 2: Steal libs
1. Pull image (fast) -or- build image from Docker (takes time)
2. Copy the libraries out of the image using a bind mount
3. Link them in your project


# Where can I download?
- `static-qt` images are [available in gitlab container registry](https://gitlab.com/octomy/static-qt/container_registry).

Alternatively, you can build from the provided Docker files yourself.


```shell
# Clone git repository
git clone git@gitlab.com:octomy/static-qt.git

```

```shell
# Pull from container registry
docker pull registry.gitlab.com/octomy/static-qt

```


# How is static-qt supposed to work?
`static-qt` works by leveraging [jinja templating engine](https://jinja.palletsprojects.com/en/) to generate a bunch of Docker files, then builds and pushes those as images with the correct tags.

Each Dockerfile is made to be a portable standalone entity so if you only need one or two targets, just go with those images/Dockerfiles.

The templates are arranged under **src/** dir while the output Docker files are output under **output/** dir

static-qt is meant to be eaasily integrated into a website to present all the working data such as Docker files, images, build scripts and release binaries for browsing and download.

# Where can I get help &amp; documentation?
What is "documentation"? No really, there is no documentation, but if you are looking for a way to build Qt statically then you are most likely able to get pretty far by just taking a peek inside the Dockerfiles.


# How can I help?
If your build fails on some combination of platform/settings then please register an issue, or even better, submit a MR!

And if I meet you in person, it goes without saying that you are buying the beverage I fancy at that moment xD
